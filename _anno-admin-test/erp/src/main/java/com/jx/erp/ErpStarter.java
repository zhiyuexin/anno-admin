package com.jx.erp;

import com.jx.erp.powerjob.PowerjobWorkerPlugin;
import lombok.extern.slf4j.Slf4j;
import org.noear.solon.Solon;
import org.noear.solon.SolonApp;
import org.noear.solon.annotation.SolonMain;
import org.noear.solon.scheduling.annotation.EnableRetry;
import org.noear.solon.scheduling.annotation.EnableScheduling;
import org.noear.solon.web.cors.CrossHandler;
import org.noear.wood.WoodConfig;
import site.sorghum.anno.anno.annotation.global.AnnoScan;
import site.sorghum.anno.anno.interfaces.CheckPermissionFunction;
import site.sorghum.anno.solon.interceptor.WoodSqlLogInterceptor;

@SolonMain
@Slf4j
@EnableRetry
@EnableScheduling
@AnnoScan(scanPackage = {"com.jx.erp", "site.sorghum.anno", "tech.powerjob.server.solon"})
public class ErpStarter {
    public static void main(String[] args) {
        SolonApp start = Solon.start(ErpStarter.class, args, app -> {
            //执行后打印sql
            WoodConfig.onExecuteAft(new WoodSqlLogInterceptor());
            app.before(-1, new CrossHandler().allowedOrigins("*").allowedHeaders("*").allowedMethods("*"));
            app.pluginAdd(1, new PowerjobWorkerPlugin());
        });
        // 忽略登录检查 (仅测试用)
        CheckPermissionFunction.loginCheckFunction = () -> {
            // StpUtil.checkLogin();
        };
        // 忽略权限检查 (仅测试用)
        CheckPermissionFunction.permissionCheckFunction = (code) -> {
            // log.info("===[Anno] permission check: {}", code);
            // StpUtil.checkPermission(code);
        };
    }
}
