package com.jx.erp.modular.hr;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.noear.wood.annotation.Table;
import site.sorghum.anno.anno.annotation.clazz.AnnoMain;
import site.sorghum.anno.anno.annotation.clazz.AnnoPermission;
import site.sorghum.anno.anno.annotation.field.AnnoEdit;
import site.sorghum.anno.anno.annotation.field.AnnoField;
import site.sorghum.anno.anno.annotation.field.AnnoSearch;
import site.sorghum.anno.anno.annotation.field.type.AnnoOptionType;
import site.sorghum.anno.anno.enums.AnnoDataType;
import site.sorghum.anno.suppose.model.BaseMetaModel;

/*
 *  @author JiangXin
 *  @since 2023/10/24 下午5:08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AnnoMain(name = "员工信息",
    annoPermission = @AnnoPermission(enable = true, baseCode = "employee", baseCodeTranslate = "员工信息"))
@Table("employee")
public class Employee extends BaseMetaModel {
    @AnnoField(title = "性别", tableFieldName = "floor", search = @AnnoSearch(),
        dataType = AnnoDataType.OPTIONS,
        optionType = @AnnoOptionType(sql = ""),
        edit = @AnnoEdit(placeHolder = "请选择性别", notNull = true))
    private String floor;

    @AnnoField(title = "姓名", tableFieldName = "name", search = @AnnoSearch(),
        edit = @AnnoEdit(placeHolder = "请输入姓名", notNull = true))
    private String name;
    @AnnoField(title = "性别", tableFieldName = "gender", search = @AnnoSearch(),
        dataType = AnnoDataType.OPTIONS,
        optionType = @AnnoOptionType(value = {
            @AnnoOptionType.OptionData(label = "男", value = "1"),
            @AnnoOptionType.OptionData(label = "女", value = "0")
        }),
        edit = @AnnoEdit(placeHolder = "请选择性别", notNull = true))
    private Integer gender = 1;
    @AnnoField(title = "身份证号", tableFieldName = "idcard", search = @AnnoSearch(),
        edit = @AnnoEdit(placeHolder = "请输入身份证号", notNull = true))
    private String idcard;
    @AnnoField(title = "手机号", tableFieldName = "mobile", search = @AnnoSearch(),
        edit = @AnnoEdit(placeHolder = "请输入手机号", notNull = true))
    private String mobile;
}
